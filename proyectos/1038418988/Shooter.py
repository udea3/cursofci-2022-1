    #--------------------------------------------------------------------------
#-------Shooter aplicando POO y conceptos básicos de PDI.------------------
#------- Por: Verónica Cardona Gómez   veronica.cardonag1@udea.edu.co -----
#-------      CC 1038418988  ----------------------------------------------
#------- Curso: Física computacional  -------------------------------------
#------- 01 de noviembre, 2022---------------------------------------------
#--------------------------------------------------------------------------

import cv2,pygame,random # Importar librerías
import numpy as np
import os 

# DEFINICIÓN DE CONSTANTES
WIDTH = 660 # Ancho y alto de la pantalla
HEIGHT = 460
BLACK = (0, 0, 0) # Colores
WHITE = ( 255, 255, 255)
GREEN = (0, 255, 0)
xa = 0
x = 0
iR = 165
iB = 155

# :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# ::::::::::::::::::::::: CONFIGURACIÓN VENTANA INICIAL ::::::::::::::::::::::: 
# :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#os.environ['SDL_VIDEO_WINDOW_POS']= '%d,%d' %(0,25)
pygame.init() # Inicializar pygame
pygame.mixer.init() # Inicializar sonidos
screen = pygame.display.set_mode((WIDTH, HEIGHT)) # Creación de la ventana ancho 800 y alto 600
pygame.display.set_caption("Shooter") # Título de la ventana
clock = pygame.time.Clock() # Reloj para controlar las fps
cam = cv2.VideoCapture(0) # Inicia la captura de videos por cámara
EE = np.ones((5,5),np.uint8) # Elemento estructurante, elimina basura
band = True # Variable booleana

# :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# ::::::::::::::::::::::::: DEFINICIÓN DE FUNCIONES  :::::::::::::::::::::::::: 
# :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


def draw_text(surface, text, size, x, y): # Función para dibujar texto, 
    font = pygame.font.SysFont("serif", size) # Fuente
    text_surface = font.render(text, True, WHITE)  # Renderizar el texto
    text_rect = text_surface.get_rect() # Obtenemos la posición actual
    text_rect.midtop = (x, y) # Posicionamos
    surface.blit(text_surface, text_rect) # Lo dibujamos en la ventana del juego
 
def draw_shield_bar(surface, x, y, percentage): # Función para ubicar la barra de escudo (sitio, (coordenadas),porcentaje)
    BAR_LENGHT = 100 # Longitud de la barra
    BAR_HEIGHT = 10  # Altura de la barra
    fill = (percentage / 100) * BAR_LENGHT # Relleno
    border = pygame.Rect(x, y, BAR_LENGHT, BAR_HEIGHT) # Borde blanco para dar la ilusión de que se está llenando
    fill = pygame.Rect(x, y, fill, BAR_HEIGHT) # Rellenado de pygame
    pygame.draw.rect(surface, GREEN, fill) # Color del relleno
    pygame.draw.rect(surface, WHITE, border, 2) # Color del borde 2: grosor del borde
    
def show_go_screen(): # Función para game over 
    screen.blit(background, [0,0])  # Colocando el fondo
    draw_text(screen, "SHOOTER", 65, WIDTH // 2, HEIGHT // 4) # Ubicando el título del juego
    draw_text(screen, "Instrucciones", 27, WIDTH // 2, HEIGHT // 2) # Ubicando las instrucciones
    draw_text(screen, "Press Key", 20, WIDTH // 2, HEIGHT * 3/4) # Ubicando presione cualquier tecla para iniciar
    pygame.display.flip() # Corregimos efecto espejo
    band = True # Creamos condicional
    waiting = True # Indica que estamos en la pantalla de espera
    while waiting and band:
        
        clock.tick(60)
        get_pic() # Función que visualiza lo que captura la cámara
        for event in pygame.event.get(): 
            if event.type == pygame.QUIT: # Si se cierra la pantalla
                cv2.destroyAllWindows() # Cierra la ventana de la cámara
                pygame.quit() # Cierra el juego
                band=False # La bandera que permite continuar es False
            if event.type == pygame.KEYUP: # Si se presiona cualquier tecla
                waiting = False # Se sale de la pantalla waiting
    return band

# :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# :::::::::::::::: INICIO DE CLASES QUE CONTROLARÁN EL JUEGO ::::::::::::::::::
# ::::::::::::::::::::::::: Y SUS PROPIEDADES :::::::::::::::::::::::::::::::::

    
class Player(pygame.sprite.Sprite): # Creación clase jugador
    def __init__(self): # Constructor
        super().__init__()
        self.image = pygame.image.load("assets/player.png").convert() #Cargando la imagen de la nave espacial
        self.image.set_colorkey(BLACK) # Ajustando el recuadro negro que encierra la nave
        self.rect = self.image.get_rect() # Obtener la línea de la nave
        self.rect.centerx = WIDTH // 2 # Centrando la nave 
        self.rect.bottom = HEIGHT - 10 # Posicionando la nave en el eje y
        self.speed_x = 0 # Inicialmente está quieta
        self.shield = 100 # Escudos inicialmente al 100%

    def left(self): # Método izquierda
        self.speed_x = -30 # Mueve 30 undidades hacia la izquierda
        self.rect.x += self.speed_x 
        if self.rect.left < 0: # Tope
            self.rect.left = 0
            
    def right(self): # Método derecha
        self.speed_x = 30 # Mueve 30 undidades hacia la derecha
        self.rect.x += self.speed_x 
        if self.rect.right > WIDTH: # Tope
            self.rect.right = WIDTH


    def shoot(self): # Método para disparar 
        bullet = Bullet(self.rect.centerx, self.rect.top) # Los argumentos son las coordenadas de la nave
        all_sprites.add(bullet) # Se agrega la bala a los sprite
        bullets.add(bullet) # agrega a balas 
        laser_sound.play() # Reproducir el sonido del laser
    
class Meteor(pygame.sprite.Sprite):
    def __init__(self):  # Constructor 
        super().__init__()
        self.image = random.choice(meteor_images) # Escogiendo de manera aleatoria de un listado de meteoros posibles
        self.image.set_colorkey(BLACK) # Quitarle el fondo negro a los meteoros
        self.rect = self.image.get_rect() # Obtener la recta del meteoro 
        self.rect.x = random.randrange(WIDTH - self.rect.width) # Pueden aparecer en cualquier parte del ancho de la pantalla
        self.rect.y = random.randrange(-140, -100) # Sólo salen desde arriba
        self.speedy = random.randrange(1, 10) # Velocidad en y aleatoria
        self.speedx = random.randrange(-5, 5) # Velocidad en x aleatoria

    def update(self):
        self.rect.y += self.speedy # Configurar la velocidad en y
        self.rect.x += self.speedx # Configurar la velocidad en x
        
        # El siguiente condicional verifica que el asteroide haya salido de la ventana
        if self.rect.top > HEIGHT + 10 or self.rect.left < -40 or self.rect.right > WIDTH + 40:
            self.rect.x = random.randrange(WIDTH - self.rect.width) # Regresando los meteoritos a la parte de arriba
            self.rect.y = random.randrange(-140, - 100)
            self.speedy = random.randrange(1, 10)
    
class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y): # Constructor
        super().__init__()
        self.image = pygame.image.load("assets/laser1.png") # Cargando imagen de la bala
        self.image.set_colorkey(BLACK) # Removiendo el fondo negro de las balas
        self.rect = self.image.get_rect() # Tomando la recta de la bala
        self.rect.y = y # Dándole la ubicación en "x" y "y"
        self.rect.centerx = x 
        self.speedy = -10 # Velocidad de la bala

    def update(self): # Hacer que suban automáticas las balas
        self.rect.y += self.speedy
        if self.rect.bottom < 0: # Cada vez que sale la bala de la ventana la eliminamos para no cargar la memoria
            self.kill()
    
class Explosion(pygame.sprite.Sprite): # Clase explosión
    def __init__(self, center): # Center: para centrar la explosión en el punto donde esté el meteorito
        super().__init__()
        self.image = explosion_anim[0] # cargamos primera imagen
        self.rect = self.image.get_rect() # Obtenemos su recta
        self.rect.center = center # La ubicamos en el parámetro centro
        self.frame = 0 # Variable para ir aumentado y simule la animación
        self.last_update = pygame.time.get_ticks() # Obtenemos el tiempo en el que estamos para saber cuándo hacer la animación
        self.frame_rate = 50 # VELOCIDAD DE LA EXPLOSION

    def update(self):
        now = pygame.time.get_ticks() # Permitirá saber cuánto tiempo ha transcurrido
        if now - self.last_update > self.frame_rate: # Si ya pasaron 50 fotogramas
            self.last_update = now # Acutalizamos el tiempo
            self.frame += 1 # Ponemos la nueva imagen
            if self.frame == len(explosion_anim): # Si se superan todas las imágenes borramos el objeto
                self.kill()
            else:
                center = self.rect.center # Tomando el centro
                self.image = explosion_anim[self.frame] # función para animar la explosion
                self.rect = self.image.get_rect() # Tomando la ubicación de la imagen
                self.rect.center = center # Centrando la image

# :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# ::::::::: TOMA Y PROCESADO DE INFORMACIÓN POR LA WEBCAM  ::::::::::::::::::::
# ::::::::::::::::::::: PARA CONTROL DE LA NAVE :::::::::::::::::::::::::::::::

def get_proc_pic(xa): # Función para obtener y procesar las imágenes de cámara
    
    ret_val, img = cam.read() #Se asigna la imagen a una variable
    img = cv2.flip(img,1) # Se corrige el efecto espejo
    
    img2 = cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb) # Se pasa de RGB a BGR2YCrCb
    
    imgR = img2[:,:,1] # Se toma la capa de rojos
    imgB = img2[:,:,2] # Se toma la capa de azules
    
    ret,bimR = cv2.threshold(imgR, iR, 255, cv2.THRESH_BINARY) # Binarización de ambas capas
    ret,bimB = cv2.threshold(imgB, iB, 255, cv2.THRESH_BINARY)
    
    bimgRO = cv2.morphologyEx(bimR, cv2.MORPH_OPEN, EE) # Aplicación a la capa roja "open" y  
    bimgROC = cv2.morphologyEx(bimgRO, cv2.MORPH_CLOSE, EE) # "close" para corregir errores
    
    bimgBO = cv2.morphologyEx(bimB, cv2.MORPH_OPEN, EE)  # Aplicación a la capa azul "open" y  
    bimgBOC = cv2.morphologyEx(bimgBO, cv2.MORPH_CLOSE, EE) # "close" para corregir errores
    
    contR, hierarchy = cv2.findContours(bimgROC, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) # Usando el método para detectar bordes disponible en opencv
    contB, hierarchy = cv2.findContours(bimgBOC, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    
    cv2.drawContours(img,contR,-1,(0,0,255),3) # Se dibujan los contornos de color rojo
    cv2.drawContours(img,contB,-1,(255,0,0),3) # Se dibujan los contornos de color rojo

    cv2.imshow('my webcam', img) # Se grafica en pantalla
    
    for c in contR: #Recorriendo los contornos encontrados
        if cv2.contourArea(c)>=50: # Los de 50 hacia abajo son objetos despreciables
            x,y,_,_=cv2.boundingRect(c) # Obtenemos la posición del contorno
            
            if (xa-x)>0: # Comparamos hacia donde fue l movimiento y realizamos la acción
                player.left() 
            if (xa-x)<0:
                player.right()
                
            xa = x # Renombramos la posición
    
    if np.count_nonzero(np.sum(bimgBOC,axis=0))>15: # Detecta si hay algún objeto de color azul apreciable
        player.shoot() # Dispara
        
    return xa

def get_pic(): # Función para sólo mostrar contornos sin mover ni disparar
    
    ret_val, img = cam.read() # Mismos comentarios que en la sección anterior 
    img = cv2.flip(img,1)
    
    img2 = cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb)
    
    imgR = img2[:,:,1]
    imgB = img2[:,:,2]
    
    ret,bimR = cv2.threshold(imgR, iR, 255, cv2.THRESH_BINARY)
    ret,bimB = cv2.threshold(imgB, iB, 255, cv2.THRESH_BINARY)
    
    bimgRO = cv2.morphologyEx(bimR, cv2.MORPH_OPEN, EE)
    bimgROC = cv2.morphologyEx(bimgRO, cv2.MORPH_CLOSE, EE)   
    
    bimgBO = cv2.morphologyEx(bimB, cv2.MORPH_OPEN, EE)
    bimgBOC = cv2.morphologyEx(bimgBO, cv2.MORPH_CLOSE, EE)
    
    contR, hierarchy = cv2.findContours(bimgROC, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contB, hierarchy = cv2.findContours(bimgBOC, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    
    cv2.drawContours(img,contR,-1,(0,0,255),3)
    cv2.drawContours(img,contB,-1,(255,0,0),3)

    cv2.imshow('my webcam', img)

# :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::    
# ::::::::::::::::::::::::: PUESTA EN MARCHA DEL JUEGO ::::::::::::::::::::::::
# :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
meteor_images = []
meteor_list = ["assets/meteorGrey_big1.png", "assets/meteorGrey_big2.png", "assets/meteorGrey_big3.png", "assets/meteorGrey_big4.png",
                "assets/meteorGrey_med1.png", "assets/meteorGrey_med2.png", "assets/meteorGrey_small1.png", "assets/meteorGrey_small2.png",
                "assets/meteorGrey_tiny1.png", "assets/meteorGrey_tiny2.png"] #Lista de nombres de todas las imágenes de meteoros
for img in meteor_list:
    meteor_images.append(pygame.image.load(img).convert()) # Lista de imagenes
    
    
explosion_anim = []  #Lista donde estarán todas las imágenes de la animación de la explosión
for i in range(9):
    file = "assets/regularExplosion0{}.png".format(i) # Nombres de los archivos
    img = pygame.image.load(file).convert() # Convirtiendo en imagen de pygame
    img.set_colorkey(BLACK) # Quitando borde negro
    img_scale = pygame.transform.scale(img, (70,70)) # Reescalando las imágenes
    explosion_anim.append(img_scale) # Rellenando la lista

background = pygame.image.load("assets/background.png").convert() # Cargar imagen de fondo

# Cargar sonidos
laser_sound = pygame.mixer.Sound("assets/laser5.ogg") # Sonido del laser
explosion_sound = pygame.mixer.Sound("assets/explosion.wav") # Sonido de la explosión
pygame.mixer.music.load("assets/music.ogg") # Música del juego 
pygame.mixer.music.set_volume(0.2) # Colocando la intensidad de la musica ambiente


#pygame.mixer.music.play(loops=-1) # Música del juego

    
game_over = True # El juego está en game over en un principio
running = True # Inicio del juego
while running and band: # Pregunta si la pantalla de inicio fue cerrada 
    
    
    if game_over: 

        band = show_go_screen() # Aplicando la función de pantalla de inicio (Se queda allí hasta que se presione una tecla)
        if band:
            game_over = False
            all_sprites = pygame.sprite.Group() # Configurando lista de sprites
            meteor_list = pygame.sprite.Group() # Configurando la lista de meteoros como grupo
            bullets = pygame.sprite.Group() # Configurando la lista de las balas
            player = Player() #Instanciando objeto jugador
            all_sprites.add(player) # Uniendo al jugador a los sprites
            for i in range(8):
                meteor = Meteor()
                all_sprites.add(meteor) # Agregando los meteoros a los sprites (Sprite: imagen 2D que forma parte de una escena gráfica mayor)
                meteor_list.add(meteor) # Agregando los meteoros a la lista de meteoros

            score = 0 #Iniciando el puntaje
            
    
    if band: # Si se mantuvo la bandera True
        xa = get_proc_pic(xa) # Se obtienen y procesan imágenes
        clock.tick(60)
        for event in pygame.event.get(): # Se monitorean los eventos del juego
            if event.type == pygame.QUIT: # Si se cierra
                cv2.destroyAllWindows() # Se destruyen las ventanas de opencv y el juego deja de correr
                running = False

            elif event.type == pygame.KEYDOWN: # Función de prueba para disparar con espacio de ser necesario
                if event.key == pygame.K_SPACE:
                    player.shoot()


        all_sprites.update() # Acutalización de sprites

        #colisiones - meteoro - laser
        hits = pygame.sprite.groupcollide(meteor_list, bullets, True, True) # Función para designar choques entre grupos
                                                             # True True es para que desaparezcan ambos grupos
        for hit in hits:
            score += 10 # Cada vez que haya un golpe aumenta el puntaje en 10
            #explosion_sound.play() # Sonido de la explosion del meteoro
            explosion = Explosion(hit.rect.center) # Instanciamos el objeto explosión
            all_sprites.add(explosion) # Lo agregamos a los sprites
            meteor = Meteor() # Volvemos a agregarlo porque lo eliminamos con la explosión
            all_sprites.add(meteor)
            meteor_list.add(meteor)

        # Checar colisiones - jugador - meteoro
        hits = pygame.sprite.spritecollide(player, meteor_list, True) # Función para designar choques entre sprites
        for hit in hits:
            player.shield -= 10 # Golpe, por lo que se disminuye la barra del escudo
            meteor = Meteor()  # Rehacer el meteoro eliminado
            all_sprites.add(meteor)
            meteor_list.add(meteor)
            if player.shield <= 0: # Si somos golpeados 10 veces
                game_over = True #GAme over

        screen.blit(background, [0, 0]) # Seteo y ubicación del background

        all_sprites.draw(screen) # Colocando el puntaje centrado arrib

        #Marcador
        draw_text(screen, str(score), 25, WIDTH // 2, 10) # Marcador de puntos

        # Escudo.
        draw_shield_bar(screen, 5, 5, player.shield) # Colocando la barra de escudo en la pantalla

        pygame.display.flip() # Corrigiendo el efecto espejo
        
pygame.quit() # Termina el juego

#--------------------------------------------------------------------------
#---------------------------  FIN DEL PROGRAMA ----------------------------
#--------------------------------------------------------------------------