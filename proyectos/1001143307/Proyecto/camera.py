import cv2
import numpy as np

EE = np.ones((5,5),np.uint8)
cam = cv2.VideoCapture(0)
xa=0

def get_pic():
    
    ret_val, img = cam.read()
    img = cv2.flip(img,1)
    img2 = cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb)
    
    imgR = img2[:,:,1]
    imgB = img2[:,:,2]
    
    ret,bimR = cv2.threshold(imgR, 165, 255, cv2.THRESH_BINARY)
    ret,bimB = cv2.threshold(imgB, 155, 255, cv2.THRESH_BINARY)
    
    bimgRO = cv2.morphologyEx(bimR, cv2.MORPH_OPEN, EE)
    bimgROC = cv2.morphologyEx(bimgRO, cv2.MORPH_CLOSE, EE)   
    
    bimgBO = cv2.morphologyEx(bimB, cv2.MORPH_OPEN, EE)
    bimgBOC = cv2.morphologyEx(bimgBO, cv2.MORPH_CLOSE, EE)
    
    contR, hierarchy = cv2.findContours(bimgROC, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contB, hierarchy = cv2.findContours(bimgBOC, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    
    cv2.drawContours(img,contR,-1,(0,0,255),3)
    cv2.drawContours(img,contB,-1,(255,0,0),3)

    cv2.imshow('my webcam', img)
    

while True:

    get_pic()
    if cv2.waitKey(1) == 27:
        break
    
cv2.destroyAllWindows() 