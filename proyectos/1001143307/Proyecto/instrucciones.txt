Hola, en este archivo se detallan las instrucciones de uso del videojuego "Shooter", para el buen funcionamiento del juego
se debe tener instalado python 3.7 en adelante, así como la librería "pygame" y "cv2", realice todo los procedimientos
necesarios y/o cerciorarse de que estén instalados en su ordenador. También asegúrese de tener una cámara web (sea
externa o incorporada) disponible. El juego no funcionará si no tiene la cámara instalada.

Paso 1

Una vez haya revisado que las dependencias necesarias están instaladas, abra el archivo "shooter" dentro de la carpeta
usando alguna IDE que le permita ejecutar códigos de python como Spyder, Visual Studio, etc. Una vez abra el archivo
ejecute el programa.

Paso 2 

Una vez ejecutado el programa, deberá esperar a que abran dos ventanas emergentes, una contedrá el juego y la otra
la vista de su cámara web. Prepare dos objetos, uno de color rojo y otro de color azuL para controlar el juego. 
Al poner estos dos objetos frente a la cámara, estos deberán ser resaltados.

Paso 3

Antes de iniciar el juego, revise en la ventana con su cámara web que no hayan objetos azules o rojos a resaltados a su 
alrededor,aparte de los que usará para el control. En el caso de que sea imposible mover algún objeto, cierre la ventana 
del juego y edite los parámetros iR e iB en las líneas 22 y 23 del código aumentándolas o disminuyéndolas en 5 puntos, 
ejecute de nuevo y vuelva al paso 2. Estos parámetros se varían para acondicionar el código a la cámara que posea.

Paso 4 

Una vez haya logrado aislar adecuadamente los objetos dispuestos para el control presione cualquier tecla y el juego
dará inicio, debe usar el movimiento del objeto rojo para mover la nave de izquierda a derecha y la aparición en cámara 
del objeto azul para disparar la misma. Cada meteoro destruido le otorgará 10 puntos y cada meteoro que impacte la nave 
le quitará 10 puntos de vida, cada que inicie la partida comenzará con 100 puntos de vida.

Paso 5

Si en algún momento desea salir, cierre la ventana donde se encuentra el juego.
