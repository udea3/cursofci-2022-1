

## importamos librerias:


import numpy as np

## A = ACIERTOS.

A = np.array([93,94.5,80.5,99.5,95.5,86.25,99,90.75,85.75,99.25,93.25])               


## R = RECHAZOS:


R = np.array([58,58.26,53.5,62.74,51.74,51.5,58.75,57.25,55.25,58.5,54])


## calculamos promedio de aciertos y rechazos:


Ap = np.mean(A)

Rp = np.mean(R)

print(" El porcentaje promedio de aciertos es,",Ap)

print(" El porcentaje promedio de rechazo es",Rp)

## Con estos porcentajes se decide el criterio para saber
## si se acepta o no el código Qr generado.

## Note que el mínimo valor de acierto es 80.5   y el máximo para rechazo es 62.74

