import numpy as np
import cv2
import matplotlib.pyplot as plt


#####################################################################################

# 3)PROCESO DE VERIFICACIÓN DE CÓDIGOS:



### leemos codigo generado y guardado:

P = cv2.imread("/home/juan/FISICA/SEMESTRE 2022-1/fisica_computacional_1/fisica_computacional_gitlab/cursofci-2022-1/proyecto_final/plantillas/12p20x20.png",0)  ## abre imagen guardada en análisis gráfico (Plantilla)


#cv2.imshow('final20x20',P)

#cv2.waitKey(0)   ## mantiene visualización de imagen por un tiempo infinito. (pres cualquier tecla para salir)

#cv2.destroyAllWindows

## abramos imagen escaneada.

N = cv2.imread("/home/juan/FISICA/SEMESTRE 2022-1/fisica_computacional_1/fisica_computacional_gitlab/cursofci-2022-1/proyecto_final/escaneadas/12scan20x20.png",0)  ## lee imagen en escala de grises (N es la nueva matriz).

Recort1 = cv2.resize(N ,(20,20))  ## reconfigura la imagen a 20 x20 pixeles.
#Recort1 = N

#cv2.imshow('1scan20x20',Recort1)

#cv2.waitKey(0) 

#cv2.destroyAllWindows

## apliquemos filtro para aumentar el contraste del código.


#print(len(Recort1))
#print(np.shape(Recort1))

## intensifiquemos los pixeles negros.

## recorramos la matriz.

for i in range(len(Recort1)):
    for j in range(len(Recort1)):

        if Recort1[i,j] < 100 :

            Recort1[i,j] = 0


#cv2.imshow('Filtrada1', Recort1)

#cv2.waitKey(0) 

#cv2.destroyAllWindows

## segundo filtro. los blancos se vuelven mas blancos:

for i in range(len(Recort1)):
    for j in range(len(Recort1)):

        if Recort1[i,j] > 100 :

            Recort1[i,j] = 255

cv2.imshow('Filtrada1', Recort1)

cv2.waitKey(0) 

cv2.destroyAllWindows
 

### comparemos matricez para ver compatibilidad:



contador =  P == Recort1 # almacena comparación entre plantilla generada y leida.

#print(contador)



suma = 0  # inicializa conteo.


for i in range(len(contador)):     ## recorre entradas de la matriz que contiene boleanos.

    for j in range(len(contador)):

        if contador[i,j] == True:

            suma = suma + 1

#print("la suma es",suma)

percent= (suma/(len(contador))**2)*100
print( "El porcentaje de aceptación es del % ",percent)

#print("dimension contador",len(contador))

#########################################################################################################

#4) ANÁLISIS ESTADÍSTICO:


## A = ACIERTOS.

A = np.array([93,94.5,80.5,99.5,95.5,86.25,99,90.75,85.75,99.25,93.25])               


## R = RECHAZOS:


R = np.array([58,58.26,53.5,62.74,51.74,51.5,58.75,57.25,55.25,58.5,54])


## calculamos promedio de aciertos y rechazos:


Ap = np.mean(A)

Rp = np.mean(R)

print(" El porcentaje promedio de compatibilidad en aciertos es,",Ap)

print(" El porcentaje promedio de compatibilidad en rechazo es ",Rp)

## Con estos porcentajes se decide el criterio para saber
## si se acepta o no el código Qr generado.

## Note que el mínimo valor de acierto es 80.5   y el máximo para rechazo es 62.74.


#### Se acepta o no el código?
## para esto , tomando el análisis estádistico se observa
## que si el porcentaje de aceptación es inferior al 65 % entonces se rechaza.
## por el contrario si es mayor al 80 % se acepta.


if percent > 80:

    print(" EL CODIGO SE ACEPTA")


if percent < 65:


    print(" EL CODIGO SE RECHAZA")


## EL criterio anterior se da que para un análisis donde las imagenes escaneadas
## eran igual a las generadas, el promedio de aceptación fue del 92.47% . mientras
## el porcentaje de rechazo tuvo un 56% de coincidencia.












