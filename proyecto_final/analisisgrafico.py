## importamos librerias

import numpy as np
import cv2
import matplotlib.pyplot as plt
import random


########################################################################################

## 1)) GENERACIÓN DE IMAGÉNES (CODIGOS)

#img1 = np.zeros((20,20), np.uint8)
#img2= 255*np.((10,10),np.uint8) ## crea matriz 10x10. aleatoria
#cv2.imshow('imgzeros',img1) ## muestra las imagenes.

# Crea matriz 20 x 20 con 1 aleatorios. ( Matriz binaria aleatoria)

Pimg1 = 255*np.random.randint(0,2,(20,20), np.uint8)  ## imagen plantilla. (Código qr creado)

# garantizamos que los 4 puntos de los extremos de la imagen son negros.
# en binario equivale 0 = negro.

## vamos a delinear los cuadrados de negro:

for k in range(len(Pimg1)):

    #lineas horizontales negras/ superior e inferior.

    Pimg1[0,k] = 0

    Pimg1[len(Pimg1)-1,k]=0

    ## lineas verticales

    Pimg1[k,0] = 0

    Pimg1[k,len(Pimg1)-1]=0



#print(Pimg)

## cambiamos tamaño de la imagen.




## muestra imagen creada con cv2:

cv2.imshow('imgplantilla20x20',Pimg1)

#cv2.resizeWindow('imgones',10,10)

cv2.waitKey(0)   ## mantiene visualización de imagen por un tiempo infinito. (pres cualquier tecla para salir)

cv2.destroyAllWindows

#################################################################################################
#2) GUARDADO Y ESCANEO DEL CÓDIGO QR:


## ahora se guarda la imagen creada en una carpeta (Proyecto_final/plantillas). con cv2.

cv2.imwrite("/home/juan/FISICA/SEMESTRE 2022-1/fisica_computacional_1/fisica_computacional_gitlab/cursofci-2022-1/proyecto_final/plantillas/12p20x20.png",Pimg1)


print(np.shape(Pimg1))
