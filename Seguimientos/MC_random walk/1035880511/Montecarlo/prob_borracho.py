import numpy as np
import matplotlib.pyplot as plt

# Encontrar la probabilidad de que el borracho quede a dos cuadras del origen al dar n pasos en 1000 intentos
numevent= 1000

# Arreglo de casos favorables
fav = []
# For que permite conseguir los arreglos en posiciones en x y en y para un número de eventos determinado
for i in range(0,numevent):
    # Arreglo de posiciones
    posx = [0]
    posy = [0]

    # Número de pasos
    N = 10


    while N > 0 :
    
        nr = np.random.randint(0,100)

        if nr <= 25:
            posy.append(posy[0]+1)
            posx.append(0)
        elif 25 < nr <= 50:
            posy.append(posy[0]-1)
            posx.append(0)
        elif 50 < nr <= 75:
            posx.append(posx[0]+1)
            posy.append(0)
        elif 75 < nr <= 100:
            posx.append(posx[0]-1)
            posy.append(0)

        
        N = N-1
    # Se genera el acumulado 
    newposx = np.cumsum(posx)
    newposy = np.cumsum(posy)

    # Condición de que quede a dos cuadras del origen
    if np.abs( newposx[ -1 ] ) + np.abs( newposy[ -1 ] ) == 2 :
        fav.append( 1 )
    else:
        fav.append( 0 )

# Cálculo de la probabilidad: #favorables/#posibles
prob = np.cumsum(fav)/np.arange(1,len(fav)+1)
#prob = len(fav)/

plt.plot(np.arange(1,len(fav)+1),prob)

plt.title('Gráfica de Probabilidad')
plt.xlabel('Intentos')
plt.ylabel('Probabilidad')
plt.show()