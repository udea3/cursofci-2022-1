
import cmagnetico as mg  # importa clase 
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

if __name__=='__main__':

    # ang = 30 grados
    # v = 5 m/s
    # B = 2 [Tesla]
    # alfha0= 0
    
    # carga =  [C]
    # masa = [Kg]
    # t = 25 [s]

    # px0, py0 , pz0 = 0

    te = np.linspace(0,40,1000)    # tiempo de 20 segundos.


    posicionx = mg.magnetico(30,5,2,0,1,5,te,0,0,0)


    #print(" La posición en x es:", posicionx.posx())

    posiciony = mg.magnetico(30,5,2,0,1,5,te,0,0,0)

    #print( " la posicion en y es:", posiciony.posy())

    posicionz = mg.magnetico(30,5,2,0,1,5,te,0,0,0)

    #print(" la posición en z es:", posicionz.posz())

    ## Grafica trayectoria en 3D

    fig= plt.figure()
    ax= Axes3D(fig)
    ax.plot(posicionx.posx(),posiciony.posy(),posicionz.posz())
    plt.grid()
 
    plt.title('fig.1 Trayectoria partícula cargada en campo magnético')
    plt.show()
    

