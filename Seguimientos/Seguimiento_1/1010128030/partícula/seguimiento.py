#OBSERVACIONES 

# 1. Definida y comentadas las clases y métodos -- OK
# 2. Configuración del entorno requirement.txt -- OK
# 3. La imagen de la trayectoria seguida por la partícula -- OK
# 4. La solución se debe subir a la carpeta Seguimientos/Seguimiento_1/SuNumeroDeCedula -- OK
# 5. BONUS -SI
# 6. Solución del problema en ecuaciones - OK
# 7. Ejecuta el código - OK 

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d 

class particula:
    
    #Constructor
    def __init__(self, energia, angulo, masa, campo, carga, iteraciones, paso):
        self.ek = energia/(6.242e18)
        self.th = angulo*(np.pi/180)
        self.m = masa
        self.b = campo/(1e6)
        self.q = carga
        self.t = np.arange(0,iteraciones/1000,paso)

    #Velocidad calculada a partir de la energía cinética, las ecuaciones utilizadas en los siguientes métodos
    #se encuentrpan adjuntas en el documento PDF
    def velocidad(self):
        v = np.sqrt((2*self.ek)/self.m)
        return v
    
    #Velocidad inicial en x
    def velocidad_equis(self):
        v = self.velocidad()
        vx = v*np.sin(self.th)
        return vx
    
    #Velocidad inicial en z
    def velocidad_zeta(self):
        v = self.velocidad()
        vz = v*np.cos(self.th)
        return vz
    
    #Posición calculada a lo largo del movimiento en la dirección x
    def posicion_equis(self):
        vx = self.velocidad_equis()
        x = (vx*self.m)/(self.q*self.b) * np.sin( (self.q*self.b *self.t)/self.m)
        return x

    #Posición calculada a lo largo del movimiento en la dirección y
    def posicion_ye(self):
        vx = self.velocidad_equis()
        y = (vx*self.m)/(self.q*self.b) * (np.cos( (self.q*self.b *self.t)/self.m) -1)
        return y

    #Posición calculada a lo largo del movimiento en la dirección z
    def posicion_zeta(self):
        vz = self.velocidad_zeta()
        z = vz*self.t
        return z
    
    #Graficación 3D
    def grafica(self):
        #mMovimiento calculado en las 3 direcciones
        x = self.posicion_equis()
        y = self.posicion_ye()
        z = self.posicion_zeta()
        #Grafica
        fig = plt.figure()
        ax = plt.axes(projection='3d')
        ax.plot3D(x, y, z)
        ax.set_title('Movimiento de la partícula')
        plt.savefig('movimiento.png')
        plt.show()
        
        
