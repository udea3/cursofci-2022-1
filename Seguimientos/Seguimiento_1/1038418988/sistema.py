#OBSERVACIONES 

# 1. Definida y comentadas las clases y métodos -- es bueno que definas todos los metodos en 
# esta clase y no en el ejecuion.py
# 2. Configuración del entorno requirement.txt -- NO
# 3. La imagen de la trayectoria seguida por la partícula -- OK
# 4. La solución se debe subir a la carpeta Seguimientos/Seguimiento_1/SuNumeroDeCedula -- OK
# 5. BONUS - NO
# 6. Solución del problema en ecuaciones - NO 
# 7. Ejecuta el código - OK 

import numpy as np
class particula:

    def __init__(self, Ek, m, q, theta, B): #Inicializamos la clase.
        self.Ek=Ek
        self.m=m
        self.q=q
        self.theta=theta
        self.B=B
    def v(self):
        return ((2*self.Ek)/self.m)**0.5 #Definimos la velocidad.
    