#OBSERVACIONES 

# 1. Definida y comentadas las clases y métodos -- falta comentar algunas funciones y clases
# 2. Configuración del entorno requirement.txt -- OK
# 3. La imagen de la trayectoria seguida por la partícula -- OK, no quedan guardadas de forma automática
# 4. La solución se debe subir a la carpeta Seguimientos/Seguimiento_1/SuNumeroDeCedula -- OK
# 5. BONUS - SI
# 6. Solución del problema en ecuaciones - OK
# 7. Ejecuta el código - OK 

import numpy as np
import matplotlib.pyplot as plt

class movement:

    #Metodo constructor
    def __init__(self, q, m, E, B, ang):
        #Inicializa los parametros
        self.q = q
        self.m = m
        self.E = E*1.6022e-19 #Convierte la energia de eV a Joules
        self.B = B
        self.ang = ang*(np.pi/180) #Convierte el angulo a radianes

        #Otros parametros
        self.wc = self.q * self.B / self.m
        self.t = np.arange(0, 40, 0.1) #Tiempo
    
    #Calcula la velocidad inicial
    def velocity(self):
        return np.sqrt((2*self.E)/self.m)

    #Calcula la posicion de la particula en x, y, z
    def x(self):
        return (self.velocity() * np.sin(self.ang) / self.wc) * np.sin(self.wc * self.t)
    
    def y(self):
        return (self.velocity() * np.sin(self.ang) / self.wc) * ( np.cos(self.wc * self.t) - 1 )
    
    def z(self):
        return self.velocity() * np.cos(self.ang) * self.t

    #Grafica las diferentes trayectorias de la particula
    def xTrayectory(self):
        plt.plot(self.t, self.x(), color = 'crimson')
        plt.xlabel('t')
        plt.ylabel('x')
        plt.title('Trayectoria en x')
        plt.show()

    def yTrayectory(self):
        plt.plot(self.t, self.y(), color = 'crimson')
        plt.xlabel('t')
        plt.ylabel('y')
        plt.title('Trayectoria en y')
        plt.show()

    def zTrayectory(self):
        plt.plot(self.t, self.z(), color = 'crimson')
        plt.xlabel('t')
        plt.ylabel('z')
        plt.title('Trayectoria en z')
        plt.show()

    def _3dTrayectory(self):
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(self.x(), self.y(), self.z(), color = 'Crimson')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        plt.suptitle('Trayectoria de la particula')
        plt.show()
