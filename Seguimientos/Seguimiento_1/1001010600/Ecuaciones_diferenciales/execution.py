import numetods as nm
import numpy as np
import sympy as sp

if __name__=="__main__":
    '''fun: Derivada de la función que se quiere solucionar'''
    #fun = lambda x, y: x + 2*y
    #fun = lambda x, y: 2*x*y
    fun = '2*x*y'
    '''Parametros iniciales'''
    x0, y0, a, b, n, x = 1, 1, 0, 1, 10, 1.5

    calcEu = nm.metodosNumericos(x0, y0, a, b, n, fun, x)

    print("El valor de la funcion con Euler en x = {} es y = {}".format(x, calcEu.euler()))
    print("El valor de la funcion con RK4 en x = {} es y = {}".format(x, calcEu.RK4()))

    '''
    No pude lograr resolver la función general de forma analitica, sympy me arroja un resultado raro y no me evalua las condiciones iniciales
    '''
    print("El valor de la funcion con la forma analitica en x = {} es y = {}".format(x, calcEu.analitic()))