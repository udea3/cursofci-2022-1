

from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
import numpy as np
from particulaCampoMagnetico import particulaCampoMagnetico

if __name__ == '__main__':

    #Rango del tiempo para graficar
    T = np.arange(0,5,0.01)

    #instancia de la clase particulaCampoMagnetico (electron)
    electron = particulaCampoMagnetico(18.6,30,9.11e-31,-1.602e-19,0.0006)

    #posiciones (x,y,z) en cada instante de tiempo en T para el electron
    x = [electron.x(t) for t in T]
    y = [electron.y(t) for t in T]
    z = [electron.z(t) for t in T]

    #plot en 3 dimensiones de la trayectoria
    fig = plt.figure()
    ax = plt.axes(projection = '3d')
    ax.plot3D(x, y, z)
    ax.set_title("Trayectoria del electrón en presencia de un campo Mágnetico",fontsize=14,fontweight="bold")
    ax.set_xlabel("X [m]")
    ax.set_ylabel("Y [m]")
    ax.set_zlabel("Z [m]")
    plt.show()