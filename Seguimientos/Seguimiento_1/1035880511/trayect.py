#OBSERVACIONES 

# 1. Definida y comentadas las clases y métodos -- OK, comentar más respecto a las variables
# 2. Configuración del entorno requirement.txt -- OK
# 3. La imagen de la trayectoria seguida por la partícula -- OK, no se guarda la imagen automáticamente
# 4. La solución se debe subir a la carpeta Seguimientos/Seguimiento_1/SuNumeroDeCedula -- OK
# 5. BONUS -SI
# 6. Solución del problema en ecuaciones - OK
# 7. Ejecuta el código - OK 

import numpy as np
import matplotlib as plt

# Se define la clase.
class Trayect:
    # Inicialización.
    def __init__(self,B,ek,theta,m,q,t):
        self.B=B
        self.ek=ek
        self.theta=theta
        self.m=m
        self.q=q
        self.t=t
    # Se definen métodos para las coordenadas y las velocidades
    def vx(self):
        v0= np.sqrt(2*self.ek/self.m)
        wc=(self.q*self.B)/self.m
        return v0*np.sin(self.theta)*np.cos(wc*self.t)
    def x(self):
        v0= np.sqrt(2*self.ek/self.m)
        wc=(self.q*self.B)/self.m
        return v0*np.sin(self.theta)/wc * np.sin(wc*self.t)
    def vy(self):
        v0= np.sqrt(2*self.ek/self.m)
        wc=(self.q*self.B)/self.m
        return -v0*np.sin(self.theta) * np.sin(wc*self.t)
    def y(self):
        v0= np.sqrt(2*self.ek/self.m)
        wc=(self.q*self.B)/self.m
        return v0*np.sin(self.theta)/wc * (np.cos(wc*self.t)-1)
    def vz(self):
        v0= np.sqrt(2*self.ek/self.m)
        return v0*np.cos(self.theta)
    def z(self):
        v0= np.sqrt(2*self.ek/self.m)
        return v0*np.cos(self.theta)*self.t
    # Se define el método "heli" para visualizar la trayectoria.
    def heli(self):
        wc=(self.q*self.B)/self.m
        v0= np.sqrt(2*self.ek/self.m)
        return np.sqrt((v0*np.sin(self.theta)/wc)**2 - (self.y()+v0*np.sin(self.theta)/wc)**2) , -np.sqrt((v0*np.sin(self.theta)/wc)**2 - (self.y()+v0*np.sin(self.theta)/wc)**2)
    



