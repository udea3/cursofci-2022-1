# MC  

import random 
import numpy as np


## El problema se reduce a definir 2 circulos de radio r colocados aleatoriamente en un plano circular.
# los circulos representan la sección transversal de las particulas, las cuales si se llegasen a cruzar
# en algún punto garantizan que las particulas colisionan. como las particulas se lanzan en direcciones 
# contrarias y sus trayectorias no cambian ( trayectorias circulares). entonces el problema se simplifica
# a estudiar las posiciones de estos circulos pequeños en el interior del circulo grande ( Seccion transversal)
# del colisionador de radio R.

## primero se definen variables:

# las posiciones se miden desde la seccion transversal del cilindro de radio R.


r = 0.1 # radio de la particula.[m]

R = 1 # radio de cilindro colisionador.[m]

A = []  # Almacena aciertos (hubo colisión.)
 
# Como r1 y r2 son las posiciones medidas desde el origen al centro de masa de cada una de las 
# particulas de radio r. estas pueden ser colocadas aleatoriamente utilizando un generador de
# numero aleatorios para las coordenadas x y y de cada partícula.
# la condicion es que estas coordenadas deben estar dentro del círculo de radio R.

N = 100000 # número de experimentos a realizar.


for k in range(N):


    ## particula 1.

    x1= random.uniform(-R,R) # genero numeros entre -R y R. en x.
    y1= random.uniform(-R,R) # /       /      //    en y.

    ## particula 2.

    x2= random.uniform(-R,R) # genero numeros entre -R y R. en x.
    y2= random.uniform(-R,R) # /       /      //    en y.

    ## apliquemos filtro ,pues las coordenadas deben estar dentro del circulo de radio R.

    if x1**2 + y1**2 < R**2 and x2**2 + y2**2 < R**2:  # condicion para que las 2 coordenadas esten dentro del circulo radio R.

        x1f = x1 ## a la variable x1f le asigna la variable x1.

        y1f = y1     # para y1.

        x2f = x2 ## a la variable x2 le asigna la variable x2.

        y2f = y2    # para y2.

       # print("coord particula 1:",x1f,y1f)
        #print("coord particula 2:",x2f,y2f)


        d =  np.sqrt( (x2f-x1f)**2 + (y2f-y1f)**2) # distancia entre centros de masa de las particulas 1 y 2.


        if d < 2*r:  ## condición importante si la distancia entre los centro de las particulas, es menor a 2r,entonces colisionan.

            A.append(1) # almacena si hubo colisión.

        #else:

             #A.append(0)


#print(A)

## calcula prob de colision. #aciertos / # experimentos.

prob = (len(A)/N)

print("La prob de que colisione la particula, durante 100000 exp es de :",prob )

print(" Notese que tomando el ejemplo para particulas de 2 milímetros en un tunel de radio R=0.5 metros, la probabilidad de que colisionen es extremadamente baja,tomando un total de cien mil experimentos")


##ANALISIS:

# LA PROBABILIDAD obtenida respecto al modelo lineal es la misma, esto debido a que no importa la trayectoria
# que tomen las particulas si al final la trayectoria es paralela desde el punto en que salio.
# las posiciones en el ejemplo del colisionador recto son las misma en el colisionandor curvo, la diferencia radica es el tiempo y lugar de colision
# pero la prob seguira siendo la misma ya que las posiciones sobre el plano circular se mantienen. 
# estas posiciones en el plano que corta al tunel serán las mismas sobre la trayectoria circular y recta.






