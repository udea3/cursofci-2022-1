import random
import numpy as np

n=0
N=10000 #Nùmero de lanzamientos.
rc=100 #Radio del cilindro.
rp=0.1 #Radio de la partìcula.
re=1 #Radio externo del colisionador circular.
ri=0.9 #Radio interno del colisionador circular.
p=[] #Lista vacìa para añadir las probabilidades.
while n<N:

    while True: 
        x1=random.uniform(-rc+rp,rc-rp) #Se generan las coordenadas aleatorias.
        y1=random.uniform(-rc+rp,rc-rp)
        x2=random.uniform(-rc+rp,rc-rp)
        y2=random.uniform(-rc+rp,rc-rp)

        if np.sqrt(x1**2+y1**2)<(rc-rp) and np.sqrt(x2**2+y2**2)<(rc-rp):
            break #Nos aseguramos de que las coordenadas estrèn dentro del cilindro.
    r1=random.uniform(ri+rp,re-rp) #Generamos una coordenada adicional para el radio del colisionador.
    r2=random.uniform(ri+rp,re-rp)

    if x1==x2 and y1==y2 and r1==r2: #Se chocan las partìculas. Favorable.
        p.append(1) 
    else: #No se chocan las particulas. Desfavorable.
        p.append(0)
    
    n+=1


f=0 #Casos favorables.
for i in range(len(p)): #Calculamos la probabilidad en cada intento.
    if p[i]==1: #Si es favorable.
        f+=1

print(f/N) #El resultado es 0.0, al igual que para el experimento.