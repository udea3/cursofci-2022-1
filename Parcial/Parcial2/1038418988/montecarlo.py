import random
import matplotlib.pyplot as plt
import numpy as np

n=10000 #Nùmero de iteraciones.

N1=2 #Nùmeros de dados.
N2=3
N3=4
X1=list(np.arange(N1,N1*6+1)) #Valores de suma posible --> para dos dados.
X2=list(np.arange(N2,N2*6+1))#Para tres dados.
X3=list(np.arange(N3,N3*6+1))#Para cuatro dados.
Y1=[] #Listas vacìas para añadir las sumas resultantes.
Y2=[]
Y3=[]
i=0
while i<=n:
    d1=random.randint(1,6) #Valores aleatorios para los dados, lanzados de manera consecutiva.
    d2=random.randint(1,6)
    d3=random.randint(1,6)
    d4=random.randint(1,6)
    sum1=d1+d2 #Sumas de los valores --> Dos dados
    sum2=d1+d2+d3 #Tres dados.
    sum3=d1+d2+d3+d4 #Cuatro dados.
    Y1.append(sum1) #Se añaden las sumas a una lista de sumas.
    Y2.append(sum2)
    Y3.append(sum3)
    i+=1

plt.title("Tabla de distribuciòn para dos dados.") #Se realizan histogramas para cada caso.
plt.hist(Y1,bins=X1)
plt.show()
plt.title("Tabla de distribuciòn para tres dados.")
plt.hist(Y2,bins=X2)
plt.show()
plt.title("Tabla de distribuciòn para cuatro dados.")
plt.hist(Y3,bins=X3)
plt.show()









