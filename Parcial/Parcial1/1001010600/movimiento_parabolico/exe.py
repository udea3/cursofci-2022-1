# COMENTARIOS

# 1. Punto 1 -- 5
# 2. PUNTO 2 -- 2 Cuando se cambia los parámetros la gráfica no cambia y el plot no es lo que se espera.
# 3. PUNTO 3 -- 0 -> No se tiene el punto 
# 4. PUNTO 4 -- 5


# En general un código bien estructurado y comentado correctamente, las soluciones 
# numéricas son correctas.


import numpy as np
import parab as pb
import matplotlib.pyplot as plt

if __name__=="__main__":
    '''
    Metodos de la clase yMov que tambien hereda la clase xMov:
    Vel: Calcula la velocidad en el respectivo eje
    tVuelo: Calcula el tiempo de vuelo
    Pos: Calcula las posiciones desde la posicion inicial hasta la posicion final
    maxPos: Calcula el alcance maximo
    '''

    x0, y0, ang, v0, ax, ay = 0, 0, 30, 10, 0.1, 9.8 #Parametros iniciales
    t = 10 #tiempo donde calcula la velocidad
    movY = pb.xMov(y0, ang, v0, ay, t)

    movX = pb.xMov(x0, ang, v0, ax, t)

    plt.plot(movX.Pos(), movY.Pos(), color = 'darkcyan')
    plt.title("Trayectoria en xy")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.savefig('Trayectoria en xy.png')
    #movX.Trayectory()
    
    