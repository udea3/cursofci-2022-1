

from TiroParabolico import TiroParabolico
from TiroParabolico import GraficaTiroParabolico
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    
    velocidad_inicial = 5
    angulo = 45   # grados
    altura_inicial = 0
    aceleracion_x = -5
    
    particula1 = GraficaTiroParabolico(velocidad_inicial, angulo, altura_inicial, aceleracion_x)
    particula1.GraficaXY()