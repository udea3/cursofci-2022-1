from PenduloNoLineal import *
import matplotlib.pyplot as plt

if __name__ == '__main__':

    tiempo_inicial = 0
    angulo_inicial = 3.1
    velocidad_inicial = 0
    longitud_pendulo = 1
    tiempo_total = 20

    pendulo = PenduloNoLineal(tiempo_inicial, angulo_inicial, velocidad_inicial, 
                               longitud_pendulo, tiempo_total)

    h = 0.001
    
    Des_ang , t = pendulo.DesplazamientoAngular(h)
    plt.plot(t, Des_ang)
    plt.title(f'Desplazamiento angular del pendulo simple soltado desde un angulo {angulo_inicial}°')
    plt.xlabel('t [s]')
    plt.ylabel('Desplazamiento Angular u [rad]')
    plt.show()