
# COMENTARIOS

# 1. Punto 1 -- 5
# 2. PUNTO 2 -- 5 
# 2. PUNTO 3 -- 5 
# 2. PUNTO 4 -- 5 

# En general un código bien estructurado y definidas correctamente las clases y métodos

import numpy as np
import matplotlib.pyplot as plt

class TiroParabolico:

    # Metodo constructor para definir los atributos iniciales de la clase
    def __init__(self, vi, angulo, altura, ax):
        # Velocidad inicial del objeto
        self.vi = vi  
        # Angulo inicial de tiro del objeto respecto al suelo, pasandolo a radianes
        self.angulo = angulo * np.pi / 180
        # Altura inicial del objeto
        self.altura = altura
        # Aceleracion en x (negativo)
        self.ax = ax
        # Aceleracion en y (gravedad) en unidades del sistema internacional de unidades
        self.g = -9.8
        # Velocidad inicial en x
        self.vix = vi * np.cos(self.angulo)
        # Velocidad incial en el eje y
        self.viy = vi * np.sin(self.angulo)
    
    # Método para calcular la velocidad en x en un tiempo t
    def vx(self, t):
        vx = self.vix + self.ax * t
        return vx

    # Método para calcular la velocidad en el eje y en un tiempo t
    def vy(self, t):
        vy = self.viy + self.g * t
        return vy
    
    # Método para el alcanze máximo del objeto en el eje x
    def xmax(self):
        xmax = -2 * self.viy * self.vix / self.g + (1 / 2) * self.ax * (2 * self.viy / self.g) ** 2
        return xmax
    
    # Método para calcular la altura maxima del objeto
    def ymax(self):
        ymax = - self.viy ** 2 / (2 * self.g)
        return ymax

    # Método para calcular el tiempo de vuelo
    def t_vuelo(self):
        t_vuelo = -2 * self.viy / self.g
        return t_vuelo
    
    # Posicion en x del objeto en un tiempo t
    def x(self, t):
        x = 0 + self.vix * t + (1 / 2) * self.ax * t ** 2
        return x

    # Posicion en y del objeto en un tiempo t
    def y(self, t):
        y = self.altura + self.viy * t + (1 / 2) * self.g * t ** 2
        return y

# Clase para graficar el plano XY de la trayectoria de la particula, que hereda la clase TiroParabolico
class GraficaTiroParabolico(TiroParabolico):
    # Heredando los atributos de la clase TiroParabolico
    def __init__(self, vi, angulo, altura, ax):
        super().__init__(vi, angulo, altura, ax)

    # Metodo para graficar el tiro parabolico en X,Y
    def GraficaXY(self):
        # Intervalo de tiempo a calcular cada punto xy del objeto
        paso = 0.01
        # Array desde el tiempo 0 hasta el tiempo de vuelo con pasos de 'paso'
        tiempo = np.arange(0, self.t_vuelo() + paso, paso)
        # Lista con las posiciones del objeto en X en cada instante de t del array tiempo
        x = [self.x(t) for t in tiempo]
        # Lista con las posiciones del objeto en Y en cada instante de t del array tiempo
        y = [self.y(t) for t in tiempo]
        # Graficando XY del objeto
        plt.plot(x, y)
        plt.title(f'Trayectoria de la particula con ax={self.ax}')
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.show()





        
    