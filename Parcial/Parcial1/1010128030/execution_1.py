# COMENTARIOS

# 1. Punto 1 -- 5
# 2. PUNTO 2 -- 5
# 3. PUNTO 3 -- 5 
# 4. PUNTO 4 -- 5


# En general un código bien estructurado y comentado correctamente, las soluciones 
# numéricas son correctas.

from POO import *
import numpy as np

if __name__ == '__main__':
    
    #Se instancia un objeto simulando la gravedad en la tierra
    Tierra = PosicionYGrafica(vel_in=2,angulo=45,gravedad=9.8,ac_viento=20,altura=0)
    Tierra.Grafica()
