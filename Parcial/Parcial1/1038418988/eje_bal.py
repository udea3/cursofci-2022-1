# COMENTARIOS

# 1. Punto 1 -- 5
# 2. PUNTO 2 -- 4.5
# 3. PUNTO 3 -- 2.5


# En general un código bien estructurado, no ejecuta Rk para el péndulo ni tampoco tenemos los
# plots para el punto 2 y 3

from PenduloBalistico import PenduloBalistico #Se importa la clase.python
from PenduloBalistico import GiroCompleto
import numpy as np

print("INGRESE LOS SIGUIENTES VALORES:") #Se ingresan los valores necesarios.
M=float(input("Masa del bloque(kg):"))
m=float(input("Masa de la bala(kg):"))
l=float(input("Longitud del pendulo(m):"))
p=PenduloBalistico(M, m, l)

op=int(input("Elija el dato que desea conocer: \n 1. Velocidad de la bala. \n 2. Desviacion en grados. \n 3. Velocidad minima de la bala para giro completo."))
if op==1:
    theta=float(input("Ingrese la desviacion angular en grados (0,180):"))
    if 0<=theta<=90:#Se asegura que los valores ingresados esten en los rangos permitidos.
        print("La velocidad que llevaba la bala es:{} m/s".format(p.VelocidadBala(theta)))
    else:
        print("Rangos no validos de angulo, intente de nuevo.")
elif op==2:
    u1=(1+(M/m))*np.sqrt(2*9.8*l)
    u=float(input("Ingrese la velocidad de la bala en m/s:"))
    if 0<=u<=u1:
        print("La desviacion del angulo es: {} grados".format(p.DesviacionAngulo(u)))
    else:
        print("Rangos no validos de velocidad, intente de nuevo.")
elif op==3: #Para la clase que heredò.
    print("La velocidad minima de la bala es:{} m/s".format(p))
else:
    print("Ingrese una opcion correcta.")


