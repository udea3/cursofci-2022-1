import numpy as np
import matplotlib.pyplot as plt
class PenduloNoLineal:
    def __init__(self, l, h): #Inicializamos la clase.
        self.l=l
        self.h=h
    def RungeKutta(self,a,b):
        self.a=a
        self.b=b
        def f(u):
            return (-9.8*(np.sin(u))/self.l)
        V=0
        U=[self.a]

        for i in range(self.a,self.b):#Se usa Runge Kutta 4 para orden 2.
            k1=self.h*V
            l1=self.h*f(i)
            k2=self.h*(V+0.5*l1)
            l2=self.h*f(i+0.5*k1)
            k3=self.h*(V+0.5*l2)
            l3=self.h*f(i+0.5*k2)
            k4=self.h*(V+l3)
            l4=self.h*f(i+k3)
            i=(1/6)*(k1+2*k2+2*k3+k4)
            U.append(i)
            V=(1/6)*(l1+2*l2+2*l3+l4)

        return (U)
    
    def DesplazamientoAngular(self):
        U=PenduloNoLineal.RungeKutta(self.a,self.b)
        T=np.arange(self.a,self.b,self.h)
        plt.title("Grafica de desplazamiento angular.")
        plt.plot(T,U)
        plt.show()
        



            


        
        