# COMENTARIOS

# 1. Punto 1 -- 5
# 2. PUNTO 2 -- 4 --> No es claro como cambiar los parametros de entrada,
# Ejemplo la altura de inicial.
# 3. PUNTO 3 -- 5 
# 4. PUNTO 4 -- 3


# En general un código bien estructurado y comentado correctamente, las soluciones 
# numéricas son correctas.

import tiroparabolico as tp  # importa clase 
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


if __name__=='__main__':


    te = np.linspace(0,4,100)    # tiempo de 4 segundos.

    ###def __init__(self,ang,v0,px0,ax,t)  parametro de entrada.

    # ang = radianes.



    posicionx = tp.TiroParabolico((np.pi)/6.0, 10*np.sin(np.pi/6.0),-3.0,-5,te)  ##ax= -3

    print("la posicion en x es" , posicionx.posx())

    

    ## ahora se heredan caracteristicas de la posicion en x para calcular sobre y.

    # secambian los parámetros de ay, velinicial en y.


    posiciony = tp.TiroParabolico((np.pi)/6.0,10*np.cos(np.pi/6.0),50,-9.8,te)   ## altura 50 metros, gravedad 0 -9.8

    posy = posiciony.posx()


    plt.plot( posicionx.posx(), posy)
    plt.show()
    plt.grid()


    









