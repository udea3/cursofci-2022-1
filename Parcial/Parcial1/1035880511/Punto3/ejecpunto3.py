import punto3 as p3
import matplotlib.pyplot as plt
import numpy as np

# Inicialización. 
if __name__=='__main__':
    print('Inicialización')

    # Se crea el objeto "tiempo" y se le aplica el método "DesplazamientoAngular"
    tiempo= np.arange(0,10,0.1)
    pendulo = p3.Pendulonolineal(0.1,0.5*np.pi,0,0,10)
    des = pendulo.DesplazamientoAngular()
    # Gráfica
    plt.plot(tiempo,des)
    plt.xlabel("t")
    plt.ylabel("Posición")
    plt.show()
    
    
